# 项目简介
howlaisi-jenkinsfile是好来斯所有的jenkinsfile集合，文件夹内的每一个文件夹，均是jenkins中的一个folder,每一个folder下面的文件，均是一个jenkinsfile文件。

# 项目环境地址：

## 项目托管地址
### gitlab
    http://gitlab.howlaisi.com:32058/
    用户名及密码 opensource  opensource
### gitee
    https://gitee.com/ximy
    已做开源
在gitlab与gitee中间做了同步代码设置（开发工作在gitlab上面，gitlab上面设置有webhook，只要有代码提交，则会自动触发流水线，将gitlab的代码推送到gitee）
## jenkins地址
    http://jenkins.howlaisi.com:30180/

## 流水线配置
![jenkins-folder.png](jenkins-folder.png)
![121.png](121.png)
![jenkinsfile-config.png](jenkinsfile-config.png)

# 联系我
![gongzhonghao.png](gongzhonghao.png)
![wechat.png](wechat.png)

